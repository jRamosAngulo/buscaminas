package modelo;

public class CasillaVacia extends Casilla {

	private ListaCasillas listaVecinos;

	public CasillaVacia() {
		this.listaVecinos= new ListaCasillas();
	}
	public void descubrirCasilla() {
		this.abierta = true;
		// pintar la casilla vacia
		System.out.println("Casilla Vacia ("+posX+","+posY+") "+listaVecinos.numVecinos());
		listaVecinos.abrirCasillasAdyacentes();
		
	}
	
	
	public ListaCasillas getListaVecinos(){
		return this.listaVecinos;
	}
}
