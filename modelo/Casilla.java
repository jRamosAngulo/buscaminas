package modelo;

import java.util.Observable;

public abstract class Casilla {

	protected boolean abierta = false;
	protected boolean marcada = false;

	// Test
	protected int posX;
	protected int posY;

	public Casilla() {

	}

	public abstract void descubrirCasilla();
	
	public boolean getMarcada() {
		return marcada;
	}

	public boolean marcarDesmarcarCasilla() {
		if (this.marcada == true) {
			marcada = false;
		} else {
			marcada = true;
		}
				
		if (marcada) {			
			System.out.println("La casilla esta marcada");
			return true;
		} else {
			System.out.println("la casilla esta desmarcada");
			return false;
		}
	}

	public int getPosX() {
		return posX;
	}

	public int getPosY() {
		return posY;
	}

	public boolean getAbierta() {
		return abierta;
	}

	public void setX(int x) {
		posX = x;
	}

	public void setY(int y) {
		posY = y;
	}

}
