package modelo;

import java.util.ArrayList;

public class ListaCasillas {

	private ArrayList<Casilla> lista;

	public ListaCasillas() {
		lista = new ArrayList<Casilla>();
	}
	
	public void abrirCasillasAdyacentes() {
		
		for (int i = 0; i < lista.size(); i++) {
			Casilla unaCasilla = lista.get(i);
			if (!unaCasilla.getAbierta()) {
				unaCasilla.descubrirCasilla();
			}
		}
				
	}
	
	public int numVecinos(){
		return lista.size();
	}

	public void anadirVecino(Casilla pCas){
		System.out.println("Voy a ANADIR UN VECINO");
		lista.add(pCas);
		System.out.println("He anadido un vecino ");
	}
	
}
