package modelo;

public class Jugador {
	private String Nombre;

	public Jugador(String pNombre) {
		this.Nombre = pNombre;
	}

	public String getNombre() {
		return Nombre;
	}

	public void setNombre(String nombre) {
		Nombre = nombre;
	}

}
