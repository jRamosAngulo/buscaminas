package modelo;

public class CasillaFactory  {

	private static CasillaFactory miCasillaFactory;
    
	private CasillaFactory() {

	}

	public static CasillaFactory getCasillaFactory() {

		if (miCasillaFactory == null) {

			miCasillaFactory = new CasillaFactory();
		}
		return miCasillaFactory;
	}

	public Casilla createCasilla(String tipo) {
		Casilla miCasilla = null;
		if (tipo.equals("CasillaBomba")) {
			miCasilla = new CasillaBomba();
		} else if (tipo.equals("CasillaNumero")) {
			miCasilla = new CasillaNumero();
		} else if (tipo.equals("CasillaVacia")) {
			miCasilla = new CasillaVacia();
			
		}
		return miCasilla;
	}
}
