package ventanas;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import modelo.Buscaminas;
import modelo.Sesion;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.JButton;
import javax.swing.JRadioButton;

public class GUI_Login extends JFrame implements Observer{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JPanel contentPane;
	private JTextField variablenombre;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					GUI_Login frame = new GUI_Login();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public GUI_Login() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 408, 282);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);

		JLabel lblNombre = new JLabel("Nombre");

		variablenombre = new JTextField();
		variablenombre.setColumns(10);

		JLabel lblNewLabel = new JLabel("ELEGIR DIFICULTAD");

		JButton btnComenzar = new JButton("Comenzar");
		btnComenzar.setActionCommand("1");
		btnComenzar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (!variablenombre.getText().isEmpty()) {
					// String cmdComenzar = e.getActionCommand();
				//	Buscaminas.getBuscaminas().empezarPartida();
					// Destruir esta interfaz y crear la de buscaminas
					Sesion.getSesion().iniciarPartida();
					
					//JFrame buscaminas = new GUI_buscaminas();
					//dispose();
				} else{
					JOptionPane.showMessageDialog(null, "Introduce un nombre si deseas jugar");
				}
				
				
			}

		}
		);
		Sesion.getSesion().addObserver(this);
		JLabel lblBuscaminasVersion = new JLabel("BUSCAMINAS ");
		
		Buscaminas.getBuscaminas().setDificultad(1);
		JRadioButton btnFacil = new JRadioButton("F�cil");
		btnFacil.setActionCommand("1");
		btnFacil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String cmd = e.getActionCommand();
				Buscaminas.getBuscaminas().setDificultad(Integer.parseInt(cmd));
				System.out.println(Buscaminas.getBuscaminas().getDificultad());
			}

		});
		btnFacil.setSelected(true);

		JRadioButton btnNormal = new JRadioButton("Normal");
		btnNormal.setActionCommand("2");
		btnNormal.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String cmd = e.getActionCommand();
				Buscaminas.getBuscaminas().setDificultad(Integer.parseInt(cmd));
				System.out.println(Buscaminas.getBuscaminas().getDificultad());
			}

		});

		JRadioButton btnDificil = new JRadioButton("Dificil");
		btnDificil.setActionCommand("3");
		btnDificil.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String cmd = e.getActionCommand();
				Buscaminas.getBuscaminas().setDificultad(Integer.parseInt(cmd));
				System.out.println(Buscaminas.getBuscaminas().getDificultad());
			}

		});
		ButtonGroup group = new ButtonGroup();
		group.add(btnFacil);
		group.add(btnNormal);
		group.add(btnDificil);

		GroupLayout gl_contentPane = new GroupLayout(contentPane);
		gl_contentPane.setHorizontalGroup(
			gl_contentPane.createParallelGroup(Alignment.TRAILING)
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addGap(81)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
						.addComponent(btnDificil)
						.addComponent(btnNormal)
						.addGroup(gl_contentPane.createSequentialGroup()
							.addGroup(gl_contentPane.createParallelGroup(Alignment.LEADING)
								.addComponent(btnFacil)
								.addComponent(lblNewLabel, GroupLayout.PREFERRED_SIZE, 131, GroupLayout.PREFERRED_SIZE)
								.addGroup(gl_contentPane.createSequentialGroup()
									.addComponent(lblNombre)
									.addPreferredGap(ComponentPlacement.UNRELATED)
									.addComponent(variablenombre, GroupLayout.PREFERRED_SIZE, 118, GroupLayout.PREFERRED_SIZE)
									.addPreferredGap(ComponentPlacement.RELATED)))
							.addContainerGap(168, Short.MAX_VALUE))))
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addGap(156)
					.addComponent(lblBuscaminasVersion)
					.addContainerGap(158, Short.MAX_VALUE))
				.addGroup(Alignment.LEADING, gl_contentPane.createSequentialGroup()
					.addGap(150)
					.addComponent(btnComenzar)
					.addContainerGap(151, Short.MAX_VALUE))
		);
		gl_contentPane.setVerticalGroup(
			gl_contentPane.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_contentPane.createSequentialGroup()
					.addGap(9)
					.addComponent(lblBuscaminasVersion)
					.addGap(18)
					.addGroup(gl_contentPane.createParallelGroup(Alignment.BASELINE)
						.addComponent(lblNombre)
						.addComponent(variablenombre, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE))
					.addGap(18)
					.addComponent(lblNewLabel)
					.addPreferredGap(ComponentPlacement.UNRELATED)
					.addComponent(btnFacil)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnNormal)
					.addPreferredGap(ComponentPlacement.RELATED)
					.addComponent(btnDificil)
					.addGap(18)
					.addComponent(btnComenzar)
					.addContainerGap(23, Short.MAX_VALUE))
		);
		contentPane.setLayout(gl_contentPane);
	}

	@Override
	public void update(Observable arg, Object arg1) {
		// TODO Auto-generated method stub
		
		Buscaminas.getBuscaminas().empezarPartida();
		JFrame buscaminas = new GUI_buscaminas();
		buscaminas.setVisible(true);
	
		dispose();	
	}
}
